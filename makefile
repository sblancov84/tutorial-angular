USER_ID = $(shell id -u $(USER))
GROUP_ID = $(shell id -g $(USER))

MY_APP = "angular-tour-of-heroes"

docker/build:
	docker build \
		-t angular:latest \
		environments/develop

docker/lint:
	docker run \
		--rm -i \
		-e HADOLINT_IGNORE=DL3016,DL3018 \
		hadolint/hadolint < environments/develop/Dockerfile

docker/rmi:
	docker rmi angular:latest

angular/create:
	docker run \
		--rm -it \
		-v $(PWD):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng new $(MY_APP)

angular/build:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng build


angular/install_schematic:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular \
		ng add $(name)

angular/generate_service:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng generate service $(name)

angular/generate_component:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng generate component $(name)

angular/generate_module:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng generate module $(name) --flat --module=app

angular/serve:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-p 4200:4200 \
		-u $(USER_ID):$(GROUP_ID) \
		angular ng serve --host 0.0.0.0

run/sh:
	docker run \
		--rm -it \
		-v $(PWD)/$(MY_APP):/app \
		-w /app \
		-u $(USER_ID):$(GROUP_ID) \
		angular sh
