# Angular Tutorial

https://sblancov84.gitlab.io/tutorial-angular

Follow https://angular.io/guide/setup-local to setup local environment, and
make it to run with Docker :D

After use

    make angular/create

I have read some documentation:

* https://angular.io/guide/architecture
* https://angular.io/guide/file-structure
* https://angular.io/cli

These reading helps me to understand better how Angular works.

Then start with the tutorial!

* https://angular.io/tutorial/toh-pt0

Open browser and type:

http://localhost:4200/

or even better, click in the link above.


OK, one curious thing is we can create components using the CLI, for instance:

    ng generate component heroes

that makes so easy to register components in our application.

More links:

* https://angular.io/tutorial/toh-pt1
* https://angular.io/tutorial/toh-pt2

Magic!

Next steps:

* https://angular.io/tutorial/toh-pt3

Done!

I do not understan how is everything connected. I have tried to change property
hero but it does not work as I expect.

Just start the next part:

* https://angular.io/tutorial/toh-pt4

WOW this is awesome.

Continue with:

* https://angular.io/tutorial/toh-pt5#navigating-to-hero-details

Ops, we have a dashboard, a hero list and hero details, and we can navigate to
every view.

Now it is time to simulate HTTP!


Follow the next link to continue:

https://angular.io/tutorial/toh-pt6#httpclient-methods-return-one-value

CRUD = Create Read Update Delete
Read and Update completed, go for Create.

Delete and Search!

Finish Angular Tutorial!

Start to use Material UI:

https://material.angular.io/guide/getting-started

## Install material

    make angular/install_schematic name=@angular/material

Then, app was customized using a new material schematic, following:

https://material.angular.io/guide/schematics

and doing

    ng generate @angular/material:navigation menu

inside the container. Also, app code was changed accordingly to use this new
navigation feature.
